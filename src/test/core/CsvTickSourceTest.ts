import {assert} from "chai";
import {TickSource} from "../../main/core/tick/TickSource";
import {CsvTickSource} from "../../main/core/tick/CsvTickSource";
import * as asStream from "highland";
import {DefaultTickData} from "../../main/core/tick/DefaultTickData";
import Stream = Highland.Stream;
import {StreamUtils} from "../../main/core/StreamUtils";
import {PriceChangedEvent} from "../../main/core/public/PriceUpdateEvent";

describe('Csv tick source', function () {
    let tickSource: TickSource;

    beforeEach(function () {
        tickSource = new CsvTickSource(__dirname + '/data/test.csv');
    });

    it('should read tick data from file', async function () {
        // when
        let result = [];
        await tickSource.onTick(async tick => {
            result.push(tick);
        });
        await tickSource.resume();
        await wait(result);

        // then
        assert.deepEqual(result[0], new DefaultTickData(new Date('2017-01-02 22:02:00.000'), 1.045200, 1.045205));
        assert.deepEqual(result[1], new DefaultTickData(new Date('2017-01-02 22:04:00.000'), 1.045260, 1.045265));
        assert.deepEqual(result[2], new DefaultTickData(new Date('2017-01-02 22:05:00.000'), 1.045380, 1.045385));

    });

    async function wait(priceChangeEvents: PriceChangedEvent[]) {
        let start = Date.now();
        while (priceChangeEvents.length < 3 && (Date.now() - start) < 5000) await tickSource.resume();
    }
});