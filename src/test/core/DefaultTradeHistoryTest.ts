import {DefaultTradeSymbol} from "../../main/core/symbol/DefaultTradeSymbol";
import {assert} from "chai";
import {DefaultTradeHistory} from "../../main/core/history/DefaultTradeHistory";
import {Bar} from "../../main/core/history/Bar";
import {DefaultTradePrice} from "../../main/core/price/DefaultTradePrice";
// import * as sinon from "sinon";
import * as Sinon from "sinon";
import {NewBarEvent} from "../../main/core/NewBarEvent";
import {StreamUtils} from "../../main/core/StreamUtils";
import {StreamTickSource} from "../../main/core/tick/StreamTickSource";
import * as asStream from "highland";
import Stream = Highland.Stream;
import {DefaultTickData} from "../../main/core/tick/DefaultTickData";

describe('Trade', function () {
    let stream: Stream<DefaultTickData>;
    let tradeSymbol: DefaultTradeSymbol;
    let tradeHistory: DefaultTradeHistory;
    let tickSource: StreamTickSource;

    beforeEach(function () {
        let fiveMinutesAsMs: number = 5 * 60 * 1000;

        stream = asStream();
        tickSource = new StreamTickSource(stream);
        tradeSymbol = new DefaultTradeSymbol(tickSource, {pipSize: 0.0001});
        tradeHistory = new DefaultTradeHistory(tradeSymbol, fiveMinutesAsMs);
    });

    it('should aggregate results of 5 minutes', async function () {
        // given
        let startTime = new Date();

        // when
        stream.write(new DefaultTickData(startTime, 1.500000, 1.00000));
        await  tradeSymbol.resume();

        // then
        // initiate
        let bar: Bar = tradeHistory.getBar(0);
        assert.deepEqual(bar.openPrice, new DefaultTradePrice(1.500000, 1.00000));
        assert.deepEqual(bar.lowestPrice, new DefaultTradePrice(1.500000, 1.00000));
        assert.deepEqual(bar.highestPrice, new DefaultTradePrice(1.500000, 1.00000));
        assert.deepEqual(bar.closePrice, new DefaultTradePrice(NaN, NaN));

        // when
        // ask and bid rises
        stream.write(new DefaultTickData(startTime, 1.600000, 1.10000));
        await  tradeSymbol.resume();

        // then
        bar = tradeHistory.getBar(0);
        assert.deepEqual(bar.openPrice, new DefaultTradePrice(1.50000, 1.00000));
        assert.deepEqual(bar.lowestPrice, new DefaultTradePrice(1.50000, 1.00000));
        assert.deepEqual(bar.highestPrice, new DefaultTradePrice(1.60000, 1.10000));
        assert.deepEqual(bar.closePrice, new DefaultTradePrice(NaN, NaN));

        // when
        // ask rises and bit is reduced
        stream.write(new DefaultTickData(startTime, 1.54000, 1.20000));
        await  tradeSymbol.resume();

        // then
        bar = tradeHistory.getBar(0);
        assert.deepEqual(bar.openPrice, new DefaultTradePrice(1.50000, 1.00000));
        assert.deepEqual(bar.lowestPrice, new DefaultTradePrice(1.50000, 1.00000));
        assert.deepEqual(bar.highestPrice, new DefaultTradePrice(1.60000, 1.20000));
        assert.deepEqual(bar.closePrice, new DefaultTradePrice(NaN, NaN));

        // when
        // ask is reduced and bid remains the same
        stream.write(new DefaultTickData(startTime, 1.40000, 1.20000));
        await  tradeSymbol.resume();

        // then
        // should change lowest price ask
        bar = tradeHistory.getBar(0);
        assert.deepEqual(bar.openPrice, new DefaultTradePrice(1.50000, 1.00000));
        assert.deepEqual(bar.lowestPrice, new DefaultTradePrice(1.40000, 1.00000));
        assert.deepEqual(bar.highestPrice, new DefaultTradePrice(1.60000, 1.20000));
        assert.deepEqual(bar.closePrice, new DefaultTradePrice(NaN, NaN));

        // when
        // ask rises and bid is lowered
        stream.write(new DefaultTickData(startTime, 1.80000, 0.90000));
        await  tradeSymbol.resume();

        // then
        // highest ask goes up and lowest bid goes down
        bar = tradeHistory.getBar(0);
        assert.deepEqual(bar.openPrice, new DefaultTradePrice(1.50000, 1.00000));
        assert.deepEqual(bar.lowestPrice, new DefaultTradePrice(1.40000, 0.90000));
        assert.deepEqual(bar.highestPrice, new DefaultTradePrice(1.80000, 1.20000));
        assert.deepEqual(bar.closePrice, new DefaultTradePrice(NaN, NaN));

        // given
        // jumps 5 minuts
        let after5Minutes = new Date();
        after5Minutes.setTime(startTime.getTime() + 5 * 60 * 1000);

        // when
        stream.write(new DefaultTickData(after5Minutes, 1.90000, 1.80000));
        await  tradeSymbol.resume();

        let bar0 = tradeHistory.getBar(0);
        assert.deepEqual(bar0.openPrice, new DefaultTradePrice(1.50000, 1.00000));
        assert.deepEqual(bar0.lowestPrice, new DefaultTradePrice(1.40000, 0.90000));
        assert.deepEqual(bar0.highestPrice, new DefaultTradePrice(1.80000, 1.20000));
        // now  the close price is set
        assert.deepEqual(bar0.closePrice, new DefaultTradePrice(1.80000, 0.90000));
        assert.deepEqual(bar0.closeTime, after5Minutes);

        let bar1 = tradeHistory.getBar(1);
        assert.deepEqual(bar1.openPrice, new DefaultTradePrice(1.80000, 0.90000));
        assert.deepEqual(bar1.lowestPrice, new DefaultTradePrice(1.80000, 0.90000));
        assert.deepEqual(bar1.highestPrice, new DefaultTradePrice(1.90000, 1.80000));
        assert.deepEqual(bar1.closePrice, new DefaultTradePrice(NaN, NaN));

        // when / then
        assert.equal(tradeHistory.getIndexAtTime(startTime.getTime()), 0);
        assert.equal(tradeHistory.getIndexAtTime(startTime.getTime() + 4 * 60 * 1000), 0);
        assert.equal(tradeHistory.getIndexAtTime(after5Minutes.getTime()), 1);
        assert.equal(tradeHistory.getIndexAtTime(after5Minutes.getTime() + 4 * 60 * 1000), 1);

    });

    it('should call handler on new bar', async function () {
        let startTime = new Date();
        let receivedEvent: NewBarEvent = null;
        await tradeHistory.onBar(async (newBarEvent: NewBarEvent) => {
            receivedEvent = newBarEvent
        });

        // when
        stream.write(new DefaultTickData(startTime, 1.50000, 1.00000));
        await tradeSymbol.resume();

        // then
        assert.deepEqual(receivedEvent.newBar, tradeHistory.getBar(0));
        assert.isNull(receivedEvent.oldBar);

        // given
        let after5Minutes = new Date();
        after5Minutes.setTime(startTime.getTime() + 5 * 60 * 1000);

        // when
        stream.write(new DefaultTickData(after5Minutes, 1.60000, 1.10000));
        await tradeSymbol.resume();

        // then
        assert.deepEqual(receivedEvent.newBar, tradeHistory.getBar(1));
        assert.deepEqual(receivedEvent.oldBar, tradeHistory.getBar(0));

    });
});