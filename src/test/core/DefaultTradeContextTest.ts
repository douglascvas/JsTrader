import {DefaultTradeSymbol} from "../../main/core/symbol/DefaultTradeSymbol";
import {DefaultAccount} from "../../main/core/account/DefaultAccount";
import {Account} from "../../main/core/public/Account";
import {DefaultTradeContext} from "../../main/core/context/DefaultTradeContext";
import {TradeType} from "../../main/core/TradeType";
import {TradeResult} from "../../main/core/context/TradeResult";
import {assert} from "chai";
import {PositionOpenEvent} from "../../main/core/public/PositionOpenEvent";
import {PositionCloseEvent} from "../../main/core/public/PositionCloseEvent";
import {StreamTickSource} from "../../main/core/tick/StreamTickSource";
import * as asStream from "highland";
import {DefaultTickData} from "../../main/core/tick/DefaultTickData";
import {TradeServer} from "../../main/core/TradeServer";
import {LocalServer} from "../../main/core/LocalServer";
import {JsonSymbolLoader} from "../../main/core/symbol/JsonSymbolLoader";
import * as path from "path";
import {TestTickBase} from "./data/TestTickBase";
import {TickData} from "../../main/core/tick/TickData";
import Stream = Highland.Stream;

describe('Trade', function () {
    let tickSourceStream: Stream<DefaultTickData>;
    let tickSource: StreamTickSource;
    let tradeSymbol: DefaultTradeSymbol;
    let account: Account;
    let tradecontext: DefaultTradeContext;
    let prices: TickData[];
    let tradeServer: TradeServer;

    beforeEach(async () => {
        tickSourceStream = asStream();
        let symbolLoader = new JsonSymbolLoader(path.resolve(__dirname + "/../resources/symbol-templates.json"));
        let tickBase = new TestTickBase(tickSourceStream);
        tradeServer = new LocalServer(symbolLoader, tickBase);
        // tradeSymbol = new DefaultTradeSymbol(tickSource, {code: 'EURUSD', pipSize: 0.0001});
        account = new DefaultAccount(tradeServer);
        tradecontext = new DefaultTradeContext(tradeSymbol, account, tradeServer);
        await tradecontext.resume();
        prices = [];
        tradeSymbol.onPriceChange(async value => {
            prices.push(value)
        });
    });

    it('should open a Buy position by calling executeMarketOrder and hit take profit', async function () {
        // given
        await sendTick(new Date(), 1.24999, 1.00000);

        // when
        let tradeResult: TradeResult = await tradecontext.executeMarketOrder(TradeType.Buy, tradeSymbol, 38000, "test position", 5000, 14.2);
        await sendTick(new Date(), 1.25142, 1.25141);

        // then
        assert.equal(tradeResult.position.grossProfit, 43.12);
        assert.equal(tradeResult.position.commissions, -2.28);
    });

    it('should close position after take profit is reached', async function () {
        // given
        await sendTick(new Date(), 1.20000, 1.20000);

        // when
        let tradeResult: TradeResult = await tradecontext.executeMarketOrder(TradeType.Buy, tradeSymbol, 1000, "test position", 5, 5);

        // hit take profit
        await sendTick(new Date(), 1.20050, 1.20050);

        // goes down 100 pips
        await sendTick(new Date(), 1.19050, 1.19050);

        // then
        await assert.equal(tradeResult.position.grossProfit, 0.42);
        await assert.equal(tradeResult.position.commissions, -0.05);
    });

    it('should open a Buy position by calling executeMarketOrder and hit stop loss', async function () {
        // given
        await sendTick(new Date(), 1.22698, 1.00000);

        // when
        let tradeResult: TradeResult = await tradecontext.executeMarketOrder(TradeType.Buy, tradeSymbol, 10000, "test position", 33.4, 1000);
        await sendTick(new Date(), 1.22365, 1.22364);

        // then
        assert.equal(tradeResult.position.grossProfit, -27.30);
        assert.equal(tradeResult.position.commissions, -0.6);
    });

    it('should close position after stop loss is reached', async function () {
        // given
        await sendTick(new Date(), 1.20050, 1.20050);

        // when
        let tradeResult: TradeResult = await tradecontext.executeMarketOrder(TradeType.Buy, tradeSymbol, 1000, "test position", 5, 5);
        // hit take profit
        await sendTick(new Date(), 1.20000, 1.20000);
        // goes down 100 pips
        await sendTick(new Date(), 1.25000, 1.25000);

        // then
        assert.equal(tradeResult.position.grossProfit, -0.42);
        assert.equal(tradeResult.position.commissions, -0.05);
    });

    it('should open a Sell position by calling executeMarketOrder and hit take profit', async function () {
        // given
        await sendTick(new Date(), 1.29999, 1.22889);

        // when
        let tradeResult: TradeResult = await tradecontext.executeMarketOrder(TradeType.Sell, tradeSymbol, 11000, "test position", 5000, 43);
        await sendTick(new Date(), 1.22459, 1.22460);

        // then
        assert.equal(tradeResult.position.grossProfit, 38.63);
        assert.equal(tradeResult.position.commissions, -0.66);
    });

    it('should open a Sell position by calling executeMarketOrder and hit stop loss', async function () {
        // given
        await sendTick(new Date(), 1.99999, 1.24541);

        // when
        let tradeResult: TradeResult = await tradecontext.executeMarketOrder(TradeType.Sell, tradeSymbol, 1000, "test position", 56.4, 99999);
        await sendTick(new Date(), 1.25105, 1.25106);

        // then
        assert.equal(tradeResult.position.grossProfit, -4.51);
        assert.equal(tradeResult.position.commissions, -0.06);
    });

    it('should call listener if position is opened', async function () {
        // given
        let openEvent1: PositionOpenEvent = null;
        let openEvent2: PositionOpenEvent = null;
        let listener1 = (positionOpenEvent: PositionOpenEvent) => openEvent1 = positionOpenEvent;
        let listener2 = (positionOpenEvent: PositionOpenEvent) => openEvent2 = positionOpenEvent;

        await sendTick(new Date(), 1.24339, 1.22639);

        // when
        tradecontext.onPositionOpened(listener1);
        tradecontext.onPositionOpened(listener2);

        let tradeResult: TradeResult = await tradecontext.executeMarketOrder(TradeType.Sell, tradeSymbol, 6000, "test position", 10, 20);

        await sendTick(new Date(), 1.22423, 1.20423);

        assert.deepEqual(openEvent1.position, tradeResult.position);
        assert.deepEqual(openEvent2.position, tradeResult.position);
    });

    it('should call listener if position is closed', async function () {
        // given
        let closeEvent1: PositionCloseEvent = null;
        let closeEvent2: PositionCloseEvent = null;
        let listener1 = (positionCloseEvent: PositionCloseEvent) => closeEvent1 = positionCloseEvent;
        let listener2 = (positionCloseEvent: PositionCloseEvent) => closeEvent2 = positionCloseEvent;

        await sendTick(new Date(), 1.24339, 1.22639);

        tradecontext.onPositionClosed(listener1);
        tradecontext.onPositionClosed(listener2);

        // when
        let tradeResult: TradeResult = await tradecontext.executeMarketOrder(TradeType.Sell, tradeSymbol, 6000, "test position", 100, 200);

        // then
        assert.isNull(closeEvent1);
        assert.isNull(closeEvent2);

        // when
        await sendTick(new Date(), 1.22423, 1.20423);

        // then
        assert.deepEqual(closeEvent1.position, tradeResult.position);
        assert.deepEqual(closeEvent2.position, tradeResult.position);

        // when
        await sendTick(new Date(), 1.99999, 1.20423);

        // then
        assert.deepEqual(closeEvent1.position, tradeResult.position);
        assert.deepEqual(closeEvent2.position, tradeResult.position);
    });

    async function sendTick(date, ask, bid) {
        tickSourceStream.write(new DefaultTickData(date, ask, bid));
        await waitForPrice();
    }

    async function waitForPrice(timeout?) {
        return new Promise((resolve, reject) => {
            // listener.resume();
            timeout = timeout || setTimeout(() => {
                prices = [];
                reject("Timed out.");
            }, 5000);
            process.nextTick(() => {
                if (prices.length > 0) {
                    prices = [];
                    clearTimeout(timeout);
                    resolve();
                }
            })
        });
    }
});