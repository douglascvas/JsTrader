import {assert} from "chai";
import {TickSource} from "../../main/core/tick/TickSource";
import * as asStream from "highland";
import {DefaultTickData} from "../../main/core/tick/DefaultTickData";
import {DefaultTradeSymbol} from "../../main/core/symbol/DefaultTradeSymbol";
import {PriceChangedEvent} from "../../main/core/public/PriceUpdateEvent";
import {InternalTradeSymbol} from "../../main/core/symbol/InternalTradeSymbol";
import {DefaultPriceChangeEvent} from "../../main/core/price/DefaultPriceUpdateEvent";
import {DefaultTradePrice} from "../../main/core/price/DefaultTradePrice";
import {StreamUtils} from "../../main/core/StreamUtils";
import {StreamTickSource} from "../../main/core/tick/StreamTickSource";
import {TickData} from "../../main/core/tick/TickData";
import Stream = Highland.Stream;

describe('Default trade symbol', function () {

    it('should trigger event on price update', async function () {
        // given
        let tickDataList = [new DefaultTickData(new Date('2017-01-02 22:02:00.000'), 1.045200, 1.045205),
            new DefaultTickData(new Date('2017-01-02 22:04:00.000'), 1.045260, 1.045265),
            new DefaultTickData(new Date('2017-01-02 22:05:00.000'), 1.045380, 1.045385)];
        let stream = <Stream<TickData>>asStream();
        let priceChangeEvents: TickData[] = [];

        let tickSource = new StreamTickSource(stream);
        let tradeSymbol: InternalTradeSymbol = new DefaultTradeSymbol(tickSource);
        tradeSymbol.onPriceChange(async tickData => {
            priceChangeEvents.push(tickData);
        });

        // when
        stream.write(tickDataList[0]);
        await tradeSymbol.resume();

        // then
        assert.deepEqual(priceChangeEvents[0], new DefaultTickData(new Date('2017-01-02 22:02:00.000'),1.045200, 1.045205));

        // when
        stream.write(tickDataList[1]);
        await tradeSymbol.resume();
        stream.write(tickDataList[2]);
        await tradeSymbol.resume();

        // then
        assert.deepEqual(priceChangeEvents[1], new DefaultTickData(new Date('2017-01-02 22:04:00.000'), 1.045260, 1.045265));
        assert.deepEqual(priceChangeEvents[2], new DefaultTickData(new Date('2017-01-02 22:05:00.000'), 1.045380, 1.045385));

    });

    async function wait(priceChangeEvents: PriceChangedEvent[]) {
        let start = Date.now();
        while (priceChangeEvents.length < 3 && (Date.now() - start) < 5000) await null;
    }

});