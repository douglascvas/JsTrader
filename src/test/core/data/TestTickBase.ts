import Stream = Highland.Stream;
import {TickBase} from "../../../main/core/tick/TickBase";
import {TickData} from "../../../main/core/tick/TickData";
import {TickSource} from "../../../main/core/tick/TickSource";

export class TestTickBase implements TickBase {
    private _stream: Stream<TickData>;

    constructor(stream: Stream<TickData>) {
        this._stream = stream;
    }

    public async populate(tickSource: TickSource, tradeSymbol: string) {
        throw new Error("not implemented");
    }

    public async ticksByDate(symbolCode: string, from: Date, to: Date): Promise<Stream<TickData>> {
        return this._stream;
    }

}