// import {TradeType} from "./TradeType";
//
// export interface Position {
//     symbolCode: number;
//     tradeType:  TradeType;
//     volume: number;
//     id: number;
//     grossProfit: number;
//     entryPrice: number;
//     stopLoss: number;
//     takeProfit: number;
//     netProfit: number;
//     swap: number;
//     commissions: number;
//     DateTime
//     entryTime;
//     pips: number;
//     label: string;
//     comment: string;
//     quantity: number;
// }

import {Server} from "./client/Server";
import {CsvTickSource} from "./core/tick/CsvTickSource";
import * as path from "path";
import {JsonSymbolLoader} from "./core/symbol/JsonSymbolLoader";
import {LocalServer} from "./core/LocalServer";
import {MongoTickBase} from "./core/tick/MongoTickBase";
import {MongoManager} from "./core/database/MongoManager";

// sourceMapSupport.install();

// let accountService = new FileAccountService(path.resolve(__dirname + "/../main/resources/test-accounts.json"));
// let trade = new Trade(new JsonSymbolLoader(path.resolve(__dirname + "/resources/symbol-templates.json")), accountService);
let tickSource = new CsvTickSource(path.resolve("C:\\Users\\douglas\\Downloads\\calgo\\exp-EURUSD-ticks.csv"));
let symbolLoader = new JsonSymbolLoader(path.resolve(__dirname + "/resources/symbol-templates.json"));
let mongoManager: MongoManager = new MongoManager();
new Server(new LocalServer(symbolLoader, new MongoTickBase(mongoManager)));