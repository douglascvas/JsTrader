$(function () {
    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    let connection = new WebSocket('ws://127.0.0.1:8081');

    connection.onopen = function () {
        // connection is opened and ready to use
        console.log('connected to websocket')
    };

    connection.onerror = function (error) {
        // an error occurred when sending/receiving data
    };

    connection.onmessage = function (message) {
        // try to decode json (I assume that each message
        // from server is json)
        console.log("message");
        try {
            let json = JSON.parse(message.data);
            $('#chart').text(JSON.stringify(json));
        } catch (e) {
            console.log('This doesn\'t look like a valid JSON: ',
                message.data);
            return;
        }
        // handle incoming message
    };
});