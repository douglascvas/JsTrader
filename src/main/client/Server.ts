import * as express from "express";
import * as WebSocket from "ws";
import * as http from "http";
import * as path from "path";
import * as bodyParser from "body-parser";
import {TickBase} from "../core/tick/TickBase";
import {MongoTickBase} from "../core/tick/MongoTickBase";
import {TradeServer} from "../core/TradeServer";
import * as cookieParser from "cookie-parser";

export class Server {
    private _tradeServer: TradeServer;
    private _tickBase: TickBase;

    constructor(tradeServer: TradeServer) {
        this._tickBase = new MongoTickBase();
        this._tradeServer = tradeServer;
    }

    public async start() {
        process.on('uncaughtException', function (err) {
            console.log(err);
        });

        let app = express();
        app.use('/', express.static(path.join(__dirname, 'public')));
        app.use(bodyParser.urlencoded({extended: false}));
        app.use(bodyParser.json());
        app.use(cookieParser());

        let websocket = null;

        const server = http.createServer(app);
        const wss = new WebSocket.Server({server, port: 8081});


        // app.post("/authenticate", (req, res, next) => {
        //     // TODO implement this
        //     next();
        // });
        //
        // app.get("/symbols", async (req, res, next) => {
        //     // res.send(await this._trade.loadSymbolTemplates());
        // });
        //
        // app.get("/populate", async (req, res, next) => {
        //     // await this._tickBase.populate(this._tradeServer., 'EURUSD');
        // });
        //
        // app.get("/accounts/{accountId}/contexts/{contextId}/bar", async (req, res, next) => {
        //     let authToken = req.cookies['auth-token'];
        //     let account: Account = await this._tradeServer.getAccountForAuthToken(authToken, req.params.accountId);
        //     if(!account){
        //         res.write("Invalid account.");
        //         res.sendStatus(400);
        //         return;
        //     }
        //     let context = account.getContext(req.params.contextId);
        //     if(!context){
        //         res.write("Invalid context.");
        //         res.sendStatus(400);
        //         return;
        //     }
        //
        //     let timeframe = req.query.timeframe;
        //     let from = new Date(req.query.from);
        //     let to = new Date(req.query.to);
        //     let tickStream: Stream<DefaultTickData> = await this._tickBase.ticksByDate(context.tradeSymbol.code, from, to);
        //
        //     (<InternalTradeSymbol>context.tradeSymbol).useSource(new StreamTickSource(tickStream));
        //     let tradeHistory: TradeHistory = await context.getOrCreateBarHistory(timeframe);
        //
        //     tradeHistory.onBar(async (barEvent: NewBarEvent) => {
        //         let data = {
        //             event: {
        //                 id: 'get /bar',
        //                 body: {
        //                     contextId: context ? context.id : null,
        //                     newPrice: barEvent.newBar,
        //                     oldPrice: barEvent.oldBar,
        //                     time: barEvent.time
        //                 }
        //             },
        //         };
        //         websocket && await websocket.send(JSON.stringify(data));
        //     });
        //
        //     await context.resume();
        //     res.sendStatus(200);
        // });
        //
        // app.post("/accounts/{accountId}/contexts", async (req, res, next) => {
        //     let authToken = req.cookies['auth-token'];
        //     let account: Account = await this._tradeServer.getAccountForAuthToken(authToken, req.params.accountId);
        //     if(!account){
        //         res.write("Invalid account.");
        //         res.sendStatus(400);
        //         return;
        //     }
        //     let context = account.getContext(req.params.contextId);
        //     if(context){
        //         res.sendStatus(200);
        //         return;
        //     }
        //
        //     await account.createContext(req.query.symbolCode, req.query.timeframeInMs);
        //     res.sendStatus(200);
        // });

        wss.on('connection', (ws, req) => {
            websocket = ws;
            // const location = url.parse(req.url, true);
            // You might use location.query.access_token to authenticate or share sessions
            // or req.headers.cookie (see http://stackoverflow.com/a/16395220/151312)
        });

        server.listen(8080, function listening() {
            console.log('Listening on %d', server.address().port);
        });
    }
}