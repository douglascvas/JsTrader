export class ErrorCode {
    public static readonly TechnicalError = new ErrorCode("TechnicalError");
    public static readonly BadVolume = new ErrorCode("BadVolume");
    public static readonly NoMoney = new ErrorCode("NoMoney");
    public static readonly MarketClosed = new ErrorCode("MarketClosed");
    public static readonly Disconnected = new ErrorCode("Disconnected");
    public static readonly EntityNotFound = new ErrorCode("EntityNotFound");
    public static readonly Timeout = new ErrorCode("Timeout");

    constructor(public name: string){
    }
}