export class PendingOrderType {
    public static readonly Limit = new PendingOrderType("Limit");
    public static readonly Stop = new PendingOrderType("Stop");

    constructor(public name: string){
    }
}