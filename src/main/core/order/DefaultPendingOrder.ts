import {PendingOrderType} from "./PendingOrderType";
import {TradeType} from "../TradeType";
import {TradeSymbol} from "../public/TradeSymbol";
import {InternalPendingOrder} from "./InternalPendingOrder";

export class DefaultPendingOrder implements InternalPendingOrder {
    private _comment: string;
    private _symbol: TradeSymbol;

    private _symbolCode: string;
    private _tradeType: TradeType;
    private _volume: number;
    private _id: number;
    private _orderType: PendingOrderType;
    private _targetPrice: number;
    private _expirationTime: Date;
    private _stopLossPips: number;
    private _takeProfitPips: number;
    private _label: string;

    public constructor(tradeType: TradeType, symbol: TradeSymbol, volume: number, targetPrice: number, label: string,
                       stopLossPips?: number, takeProfitPips?: number) {
        stopLossPips = stopLossPips || 0;
        takeProfitPips = takeProfitPips || 0;
        this._targetPrice = targetPrice;
        this._symbol = symbol;
        this._tradeType = tradeType;
        this._volume = volume;
        this._label = label;
        this._stopLossPips = stopLossPips;
        this._takeProfitPips = takeProfitPips;
    }

    get symbolCode(): string {
        return this._symbolCode;
    }

    get tradeType(): TradeType {
        return this._tradeType;
    }

    get volume(): number {
        return this._volume;
    }

    get id(): number {
        return this._id;
    }

    get orderType(): PendingOrderType {
        return this._orderType;
    }

    get targetPrice(): number {
        return this._targetPrice;
    }

    get expirationTime(): Date {
        return this._expirationTime;
    }

    get stopLossPips(): number {
        return this._stopLossPips;
    }

    get takeProfitPips(): number {
        return this._takeProfitPips;
    }

    get label(): string {
        return this._label;
    }

    get comment(): string {
        return this._comment;
    }
}