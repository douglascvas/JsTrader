import {Node} from "./Node";

export class BinaryTree<E> {
    public root: Node<E>;

    public add(key: number, value: E): Node<E> {
        if(key == null){
            return null;
        }
        if (!this.root) {
            return this.root = new Node(key, value)
        } else {
            return this.root.add(new Node<E>(key, value));
        }
    }

    public findGreaterThan(key: number, includeEquals?: boolean): Node<E> {
        if(key == null){
            return null;
        }
        if (!this.root) {
            return null;
        }
        return this.root.findGreaterThan(key, includeEquals);
    }

    public findSmallerThan(key: number, includeEquals?: boolean): Node<E> {
        if (!this.root) {
            return null;
        }
        return this.root.findSmallerThan(key, includeEquals);
    }

    public remove(value: E) {
        this.root.remove(value);
    }

    public toList(): Node<E>[] {
        if (!this.root) {
            return [];
        }
        return this.root.toList();
    }
}