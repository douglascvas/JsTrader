export class Node<E> {
    key: number;
    value: E;
    left: Node<E>;
    right: Node<E>;

    public constructor(key: number, value: any) {
        this.key = key;
        this.value = value;
    }

    public findGreaterThan(key: number, includeEquals: boolean): Node<E> {
        if(this.key > key || (includeEquals && this.key == key)){
            return this;
        } else if(this.right != null){
            return this.right.findGreaterThan(key, includeEquals);
        }
        return null;
    }

    public findSmallerThan(key: number, includeEquals: boolean): Node<E> {
        if(this.key < key || (includeEquals && this.key == key)){
            return this;
        } else if(this.left != null){
            return this.left.findSmallerThan(key, includeEquals);
        }
        return null;
    }

    public add(node: Node<E>): Node<E>{
        if(!node){
            return;
        }
        if(node.key < this.key){
            if(this.left != null){
                return this.left.add(node);
            }else{
                return this.left = node;
            }
        }else{
            if(this.right != null){
                return this.right.add(node);
            }else{
                return this.right = node;
            }
        }
    }

    public remove(value: E){
        if(this.left){
            if(this.left.value === value){
                let left = this.left;
                this.left = null;
                this.add(left.left);
                this.add(left.right);
            } else {
                this.left.remove(value);
            }
        }
        if(this.right){
            if(this.right.value === value){
                let right = this.right;
                this.right = null;
                this.add(right.left);
                this.add(right.right);
            } else {
                this.right.remove(value);
            }
        }
    }

    public toList(list?: Node<E>[]): Node<E>[] {
        if(!list){
            list = [];
        }
        if(this.left != null){
            this.left.toList(list);
        }
        list.push(this);
        if(this.right != null){
            this.right.toList(list);
        }
        return list;
    }
}