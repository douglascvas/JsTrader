export class RoundingMode {
    public static readonly ToNearest = new RoundingMode("ToNearest");
    public static readonly Down = new RoundingMode("Down");
    public static readonly Up = new RoundingMode("Up");

    constructor(public name: string){
    }
}