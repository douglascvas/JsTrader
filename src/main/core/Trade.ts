import {TradeSymbol} from "./public/TradeSymbol";
import {TradeContext} from "./public/TradeContext";
import {SymbolTemplateLoader} from "./symbol/SymbolTemplateLoader";
import {ProcessRequest} from "./robot/ProcessRequest";
import {TradeServer} from "./TradeServer";
import {MongoClient} from "mongodb";
import {MongoManager} from "./database/MongoManager";
import {Robot} from "./public/Robot";
import {LocalServer} from "./LocalServer";
import {DefaultAccount} from "./account/DefaultAccount";
import {JsonSymbolLoader} from "./symbol/JsonSymbolLoader";
import * as path from "path";
import {MongoTickBase} from "./tick/MongoTickBase";

export class Trade {
    // TODO - remove item when symbol not being used anymore (remove timeouts)
    private _symbolLoader: SymbolTemplateLoader;
    private _tradeSymbolTemplateFromTradeSymbolCode: Map<string, TradeSymbol>;
    private _mongoManager: MongoManager;
    private _botPathFromBotId: Map<string, string>;

    private _tradeContexts: Map<string, TradeContext>;
    private _tradeSymbolFromSymbolCode: Map<String, TradeSymbol>;
    private _tradeServer: TradeServer;

    constructor(botPathFromBotId: Map<string, string>,
                tradeServer: TradeServer,
                mongoManager: MongoManager) {
        this._tradeContexts = new Map<string, TradeContext>();
        this._tradeSymbolFromSymbolCode = new Map();
        this._tradeSymbolTemplateFromTradeSymbolCode = new Map();
        this._tradeServer = tradeServer;
        this._mongoManager = mongoManager;
        this._botPathFromBotId = botPathFromBotId;
    }

    private async consumeProcessRequests(processRequest: ProcessRequest) {
        let symbolLoader = new JsonSymbolLoader(path.resolve(__dirname + "/../resources/symbol-templates.json"));
        let server = new LocalServer(symbolLoader, new MongoTickBase(this._mongoManager));
        let account = new DefaultAccount(server, processRequest.account);
        let context = await account.createContext(processRequest.symbolCode, processRequest.fromDate, processRequest.toDate);
        context.tradeSymbol.onPriceChange()

        const botPath = this._botPathFromBotId.get(processRequest.botId);
        const BotClass = require(botPath);
        const bot: Robot<any> = <any>new BotClass();
        await bot.initialize(account, processRequest.botConfiguration);

        //TODO: 1) load ticks - 2)pass each tick to bot - 3) after each tick, record balance and equity in the database


        let RobotClass = require(processRequest.botId);
        let robot = new RobotClass();
        robot.initialize(this._tradeServer, processRequest.data);
    }


    // public async loadSymbolTemplates(): Promise<TradeSymbol[]> {
    //     this._tradeSymbolTemplateFromTradeSymbolCode = await this._symbolLoader.loadSymbols();
    //     return Array.from(await this._tradeSymbolTemplateFromTradeSymbolCode.values());
    // }

    // public createContext(account: Account,
    //                           symbolCode: string,
    //                           tickSource: TickSource,
    //                           tradeConfiguration: TradeConfiguration = <TradeConfiguration>{}): TradeContext {
    //
    //     let tradeSymbol = this.getOrCreateSymbol(symbolCode, tickSource);
    //     let context: TradeContext = new DefaultTradeContext(tradeSymbol,
    //         account,
    //         tradeConfiguration);
    //     this._tradeContexts.set(context.id, context);
    //     return context;
    //     //new CsvTickSource(`/Users/douglasvasconcelos/Downloads/calgo/exp-{symbolCode}-ticks.csv`)
    //
    // }

    // public getContext(contextId: string){
    //     return this._tradeContexts.get(contextId);
    // }

    // private getOrCreateSymbol(symbolCode: string, tickSource: TickSource) {
    //     return this._tradeSymbolFromSymbolCode.get(symbolCode) ||
    //         new DefaultTradeSymbol(tickSource, this._symbolLoader.loadSymbols().get(symbolCode));
    // }
}