import {Account} from "../public/Account";

export interface ProcessRequest {
    account: Account;
    symbolCode: string;
    fromDate: Date;
    toDate: Date;
    timeframeInMs: number;
    botId: string;
    botConfiguration: object;
}