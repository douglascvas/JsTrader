import {Account} from "./public/Account";
import {TradeSymbol} from "./public/TradeSymbol";
import {TickSource} from "./tick/TickSource";

export interface TradeServer {
    //
    // constructor(symbolLoader: SymbolTemplateLoader) {
    //     this._tradeSymbolFromSymbolCode = new Map();
    //     this._symbolLoader = symbolLoader;
    // }

    loadTicks(symbolCode: string, from?: Date, to?: Date): Promise<TickSource>;

    createSymbol(symbolCode: string, tickSource: TickSource): Promise<TradeSymbol>;

    getOrCreateSymbol(symbolCode: string, tickSource: TickSource): Promise<TradeSymbol>;

    authenticate(username: string, password: string);

    getAccountsForAuthToken(authToken): Promise<Account[]>;

    getAccountForAuthToken(authToken, accountId): Promise<Account>;

    getComissionPerMillion(): Promise<number>;

}