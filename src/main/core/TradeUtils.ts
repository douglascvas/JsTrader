import {TradeSymbol} from "./public/TradeSymbol";

export class TradeUtils {
    public static readonly ROUNDING_SIZE: number = 2;

    public static pipsToPrice(pips: number, tradeSymbol: TradeSymbol): number {
        return pips * tradeSymbol.pipSize;
    }

    public static priceToPips(price: number, tradeSymbol: TradeSymbol): number {
        return price / tradeSymbol.pipSize;
    }

    public static priceToVolume(price: number, stopLossInPips: number, tradeSymbol: TradeSymbol): number {
        if (stopLossInPips === null)
            return 0;
        let stopLossValue: number = stopLossInPips;
        return (price / (stopLossValue * tradeSymbol.pipSize));
    }

    public static round(value: number, precision: number = this.ROUNDING_SIZE): number {
        let factor: number = Math.pow(10, precision);
        return Math.round(value * factor) / factor;
    }
}