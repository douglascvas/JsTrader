import {DateProvider} from "./DateProvider";

export class DefaultDateProvider implements DateProvider {
    now(): Date {
        return new Date();
    }
}