import * as asStream from "highland";
import Stream = Highland.Stream;
import {DefaultTradePrice} from "./price/DefaultTradePrice";

export class StreamUtils {
    public static async streamToArray<T>(stream: Stream<T>): Promise<T[]> {
        return <any>new Promise((resolve, reject) => {
            stream.toArray((array: T[]) => {
                resolve(array);
            });
        });
    }

    public static async pull<T>(stream: Stream<T>): Promise<T> {
        return <any>new Promise((resolve, reject) => {
            stream.pull((err, value: T) => {
                if (err) {
                    reject(err);
                }
                resolve(value);
            });
        });
    }

    public static from<T>(stream: Stream<T>): Stream<T> {
        return stream.observe();
        // let newStream: Stream<T> = asStream();
        // stream.observe().pipe(newStream);
        // return newStream;
    }

    public static asyncMap<T,R>(stream: Stream<T>, fn: (T)=>Promise<R>, onEnd?: ()=>Promise<void>): Stream<R> {
        return asStream(stream).consume(async (err, value: T, push, next) => {
            if (err) {
                push(err);
                next();
            }
            else if (value === <any>asStream.nil) {
                onEnd && await onEnd();
                push(err, <any>asStream.nil);
            }
            else {
                try {
                    push(null, await fn(value));
                }
                catch (e) {
                    push(e, null);
                }
                next();
            }
        });
    };
}