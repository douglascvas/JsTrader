import {Account} from "./public/Account";
import {TickSource} from "./tick/TickSource";
import {DefaultTradeSymbol} from "./symbol/DefaultTradeSymbol";
import {TradeSymbol} from "./public/TradeSymbol";
import {SymbolTemplateLoader} from "./symbol/SymbolTemplateLoader";
import {TradeServer} from "./TradeServer";
import {DefaultAccount} from "./account/DefaultAccount";
import {TickBase} from "./tick/TickBase";
import {StreamTickSource} from "./tick/StreamTickSource";

export class LocalServer implements TradeServer {
    private _tradeSymbolFromSymbolCode: Map<string, TradeSymbol>;
    private _accountFromAuthToken: Map<string, Map<string, Account>>;
    private _symbolLoader: SymbolTemplateLoader;
    private _tickBase: TickBase;

    constructor(symbolLoader: SymbolTemplateLoader, tickBase: TickBase) {
        this._accountFromAuthToken = new Map();
        this._symbolLoader = symbolLoader;
        this._tradeSymbolFromSymbolCode = new Map<string, TradeSymbol>();
        this._tickBase = tickBase;
    }

    public async loadTicks(symbolCode: string, from?: Date, to?: Date): Promise<TickSource> {
        return new StreamTickSource(await this._tickBase.ticksByDate(symbolCode, from || new Date(), to || null));
    }

    public async createSymbol(symbolCode: string, tickSource: TickSource): Promise<TradeSymbol> {
        let symbolTemplate: TradeSymbol = (await this._symbolLoader.loadSymbols()).get(symbolCode);
        return new DefaultTradeSymbol(tickSource, symbolTemplate);
    }

    public async getOrCreateSymbol(symbolCode: string, tickSource: TickSource): Promise<TradeSymbol> {
        let symbol = this._tradeSymbolFromSymbolCode.get(symbolCode);
        if (symbol) {
            return symbol;
        }
        return this.createSymbol(symbolCode, tickSource);
    }

    public async authenticate(username: string, password: string) {
        let accounts = require("../resources/test-accounts");
        let accountMap: Map<string, Account> = new Map();
        accounts.forEach(accTemplate => {
            accountMap.set(accTemplate.accountId, new DefaultAccount(this, require("../resources/test-accounts")));
        });
        this._accountFromAuthToken.set('123', accountMap);
    }

    public async getAccountsForAuthToken(authToken: string): Promise<Account[]> {
        return Array.from(this._accountFromAuthToken.get(authToken).values());
    }

    public async getAccountForAuthToken(authToken: string, accountId: string): Promise<Account> {
        let accMap = this._accountFromAuthToken.get(authToken);
        if (!accMap) {
            return null;
        }
        return accMap.get(accountId);
    }


    public async getComissionPerMillion(): Promise<number> {
        return 50;
    }

}