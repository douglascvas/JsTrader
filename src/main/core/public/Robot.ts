import {Account} from "./Account";

export interface Robot<E> {
    initialize(account: Account, configuration: E): Promise<void>;
}