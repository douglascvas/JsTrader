import {Position} from "./Position";
import {ErrorCode} from "../ErrorCode";

export interface PositionCloseEvent {
    position: Position;
    error: ErrorCode;
}