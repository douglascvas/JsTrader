import {Position} from "./Position";
import {TradeContext} from "./TradeContext";
import {TickSource} from "../tick/TickSource";

export interface Account {
    accountId: string;
    balance: number;
    brokerName: string;
    currency: string;
    equity: number;
    freeMargin: number;
    isLive: boolean;
    leverage: number;
    margin: number;
    marginLevel: number;
    /**
     *  All the positions opened in this account (summing up all the trade contexts)
     */
    positions: Position[];
    // unrealizedGrossProfit: number;
    // unrealizedNetProfit: number;

    createContext(symbolCode: string, from?: Date, to?: Date): Promise<TradeContext>;
    getContext(contextId: string): TradeContext;
    getContexts(): TradeContext[];
}