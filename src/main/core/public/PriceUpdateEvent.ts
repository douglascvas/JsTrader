import {TradePrice} from "./TradePrice";
import {TradeSymbol} from "./TradeSymbol";

export interface PriceChangedEvent {
    time: Date;
    tradeSymbol: TradeSymbol;
    newPrice: TradePrice;
    previousPrice: TradePrice;
}