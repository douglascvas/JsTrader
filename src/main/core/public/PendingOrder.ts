import {PendingOrderType} from "../order/PendingOrderType";
import {TradeType} from "../TradeType";

export interface PendingOrder {
    comment: string;
    id: number;
    expirationTime: Date;
    label: string;
    orderType: PendingOrderType;
    stopLossPips: number;
    symbolCode: string;
    takeProfitPips: number;
    targetPrice: number;
    tradeType: TradeType;
    volume: number;
}