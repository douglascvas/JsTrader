export interface TradePrice {
    ask: number;
    bid: number;
}