import {Position} from "./Position";
import {ErrorCode} from "../ErrorCode";

export interface PositionOpenEvent {
    position: Position;
    error: ErrorCode;
}