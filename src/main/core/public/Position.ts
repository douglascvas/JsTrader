import {TradeType} from "../TradeType";
import {TradeSymbol} from "./TradeSymbol";
import {TradeContext} from "./TradeContext";
import {TradeResult} from "../context/TradeResult";

export interface Position {
    closedAt: Date;
    comment: string;
    commissions: number;
    entryPrice: number;
    entryTime: Date;
    grossProfit: number;
    id: number;
    label: string;
    netProfit: number;
    pips: number;
    profit: number;
    stopLoss: number;
    swap: number;
    takeProfit: number;
    tradeContext: TradeContext;
    tradeSymbol: TradeSymbol;
    tradeType: TradeType;
    volume: number;
}