import {TradePrice} from "./TradePrice";
import {PriceChangedEvent} from "./PriceUpdateEvent";
import {DefaultTickData} from "../tick/DefaultTickData";
import Stream = Highland.Stream;
import {TickData} from "../tick/TickData";

export interface TradeSymbol {
    price: TradePrice;
    spread: number;
    code: string;
    pipSize: number;
    digits: number;
    volumeMin: number;
    volumeMax: number;
    volumeStep: number;
    // lotSize: number;

    resume(): Promise<void>;
    pause(): Promise<void>;
    onPriceChange(listener: (e: TickData) => Promise<void>): Function;
    onPriceChangeEnd(listener: () => Promise<void>): Function;
}