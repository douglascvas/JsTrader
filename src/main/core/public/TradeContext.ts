import {TradeType} from "../TradeType";
import {TradeResult} from "../context/TradeResult";
import {Position} from "./Position";
import {TradeSymbol} from "./TradeSymbol";
import {Account} from "./Account";
import {PositionOpenEvent} from "./PositionOpenEvent";
import {PositionCloseEvent} from "./PositionCloseEvent";
import {TradeHistory} from "../history/TradeHistory";

export interface TradeContext {
    id: string;
    tradeSymbol: TradeSymbol;
    positions: Position[];
    account: Account;

    resume(): Promise<void>;

    pause(): Promise<void>;

    getOrCreateBarHistory(timeframeInMs: number): Promise<TradeHistory>;

    onPositionOpened(listener: (event: PositionOpenEvent) => void): Function;

    onPositionClosed(listener: (event: PositionCloseEvent) => void): Function;

    executeMarketOrder(tradeType: TradeType,
                       currency: TradeSymbol,
                       volume: number,
                       label: string,
                       stopLossPips?: number,
                       takeProfitPips?: number,
                       comment?: string): Promise<TradeResult>;

    placeStopOrder(tradeType: TradeType,
                   currency: TradeSymbol,
                   volume: number,
                   targetPrice: number,
                   label: string,
                   stopLossPips: number,
                   takeProfitPips: number): Promise<TradeResult>;

    closePosition(position: Position): Promise<void>;
}
