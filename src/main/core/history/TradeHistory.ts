import {Bar} from "./Bar";
import {NewBarEvent} from "../NewBarEvent";
import Stream = Highland.Stream;

export interface TradeHistory {
    timeframeInMs: number;
    getIndexAtTime(timeInMillis: number): number;
    getBar(index: number): Bar;
    onBar(listener: (newBarEvent: NewBarEvent) => Promise<void>): Function;
}