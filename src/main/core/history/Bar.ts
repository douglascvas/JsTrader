import {TradePrice} from "../public/TradePrice";
import {DefaultTradePrice} from "../price/DefaultTradePrice";

export class Bar {
    private _openTime: Date;
    private _closeTime: Date;
    private _highestPrice: TradePrice;
    private _lowestPrice: TradePrice;
    private _openPrice: TradePrice;
    private _closePrice: TradePrice;
    private _highestSpread: number;
    private _lowestSpread: number;
    private _lastPrice: TradePrice;

    constructor(price: TradePrice, time: Date) {
        this._highestPrice = new DefaultTradePrice(price.ask, price.bid);
        this._lowestPrice = new DefaultTradePrice(price.ask, price.bid);
        this._openPrice = new DefaultTradePrice(price.ask, price.bid);
        this._closePrice = new DefaultTradePrice(NaN, NaN);
        this._openTime = time;
        this._lastPrice = new DefaultTradePrice(price.ask, price.bid);

        let spread: number = this.calculateSpread(price);
        this._highestSpread = spread;
        this._lowestSpread = spread;
    }

    public addPrice(price: TradePrice) {
        this.setAskPrice(price);
        this.setBidPrice(price);
        this.setSpread(price);
        this._lastPrice = price;
    }

    public close(time: Date): TradePrice {
        if (!this._closeTime) {
            this._closePrice = new DefaultTradePrice(this._lastPrice.ask, this._lastPrice.bid);
            this._closeTime = time;
        }
        return this._lastPrice;
    }

    private setAskPrice(price: TradePrice) {
        if (price.ask > this._highestPrice.ask) {
            this._highestPrice = new DefaultTradePrice(price.ask, this.highestPrice.bid);
        } else if (price.ask < this._lowestPrice.ask) {
            this._lowestPrice = new DefaultTradePrice(price.ask, this.lowestPrice.bid);
        }
    }

    private setBidPrice(price: TradePrice) {
        if (price.bid > this._highestPrice.bid) {
            this._highestPrice = new DefaultTradePrice(this.highestPrice.ask, price.bid);
        } else if (price.bid < this._lowestPrice.bid) {
            this._lowestPrice = new DefaultTradePrice(this.lowestPrice.ask, price.bid);
        }
    }

    private setSpread(price: TradePrice) {
        let spread = this.calculateSpread(price);

        if (spread > this._highestSpread) {
            this._highestSpread = spread;
        } else if (spread < this._lowestSpread) {
            this._lowestSpread = spread;
        }
    }

    private calculateSpread(price: TradePrice) {
        return price.ask - price.bid;
    }


    get highestPrice(): TradePrice {
        return this._highestPrice;
    }

    get lowestPrice(): TradePrice {
        return this._lowestPrice;
    }

    get openPrice(): TradePrice {
        return this._openPrice;
    }

    get closePrice(): TradePrice {
        return this._closePrice;
    }

    get highestSpread(): number {
        return this._highestSpread;
    }

    get lowestSpread(): number {
        return this._lowestSpread;
    }

    get closeTime(): Date {
        return this._closeTime;
    }

    get openTime(): Date {
        return this._openTime;
    }
}