import {TradeHistory} from "./TradeHistory";
import {TradeSymbol} from "../public/TradeSymbol";
import {Bar} from "./Bar";
import {NewBarEvent} from "../NewBarEvent";
import {DefaultNewBarEvent} from "../DefaultNewBarEvent";
import {DefaultTradePrice} from "../price/DefaultTradePrice";
import {TickData} from "../tick/TickData";
import {Observer} from "../technical/Observer";
import {TradePrice} from "../public/TradePrice";

export class DefaultTradeHistory implements TradeHistory {
    private _timeframeInMs: number;
    private _lastIndex: number;
    private _startTimeInMillis: number;
    private _barFromIndex: Map<number, Bar>;
    private _onBarObserver: Observer<NewBarEvent>;

    constructor(tradeSymbol: TradeSymbol, timeframeInMs: number) {
        this._barFromIndex = new Map();
        this._startTimeInMillis = Date.now();
        this._timeframeInMs = timeframeInMs;
        this._onBarObserver = new Observer<NewBarEvent>();

        tradeSymbol.onPriceChange(async tickData => {
            await this.priceChangeHandler(tickData);
        });
    }

    public onBar(listener: (newBarEvent: NewBarEvent) => Promise<void>): Function {
        return this._onBarObserver.subscribe(listener);
    }

    public getIndexAtTime(timeInMillis: number): number {
        return Math.floor((timeInMillis - this._startTimeInMillis) / this._timeframeInMs);
    }

    public getBar(index: number): Bar {
        return this._barFromIndex.get(index);
    }

    private async priceChangeHandler(tickData: TickData): Promise<void> {
        let index: number = this.getIndexAtTime(tickData.time.getTime());

        let bar: Bar = this._barFromIndex.get(index);
        let price: TradePrice = new DefaultTradePrice(tickData.ask, tickData.bid);
        if (!bar) {
            // Bar does not exist for the given index... it can be that it is a new bar timeframe or the first one
            let lastPrice = price;
            let lastBar = this._barFromIndex.get(this._lastIndex);
            // if there is a previous bar, close it and get the price from it
            if (lastBar) {
                lastPrice = lastBar.close(new Date(lastBar.openTime.getTime() + this._timeframeInMs));
                bar = new Bar(lastPrice, tickData.time);
                bar.addPrice(price);
            } else {
                // otherwise create the first bar
                bar = new Bar(price, tickData.time);
            }
            this._barFromIndex.set(index, bar);
            await this._onBarObserver.trigger(new DefaultNewBarEvent(bar, lastBar, tickData.time));
        } else {
            bar.addPrice(price);
        }
        this._lastIndex = index;
    }

    get timeframeInMs(): number {
        return this._timeframeInMs;
    }
}