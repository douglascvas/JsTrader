import {Bar} from "./history/Bar";
import {NewBarEvent} from "./NewBarEvent";

export class DefaultNewBarEvent implements NewBarEvent {
    private _newBar: Bar;
    private _oldBar: Bar;
    private _time: Date;

    constructor(newBar: Bar, oldBar: Bar, time: Date) {
        this._newBar = newBar;
        this._oldBar = oldBar;
        this._time = time;
    }

    get newBar(): Bar {
        return this._newBar;
    }

    get oldBar(): Bar {
        return this._oldBar;
    }


    get time(): Date {
        return this._time;
    }
}