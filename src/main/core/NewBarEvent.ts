import {Bar} from "./history/Bar";

export interface NewBarEvent {
    newBar: Bar;
    oldBar: Bar;
    time: Date;
}