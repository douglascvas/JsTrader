import {TickData} from "./TickData";

export interface TickSource {
    onTick(listener: (e: TickData) => Promise<void>): Function;
    onEnd(listener: (e: TickData) => Promise<void>): Function;
    resume(): Promise<void>;
    pause(): Promise<void>;
}