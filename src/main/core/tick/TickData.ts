export interface TickData {
    time: Date;
    ask: number;
    bid: number;
}