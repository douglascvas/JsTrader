import {TickSource} from "./TickSource";
import {StreamUtils} from "../StreamUtils";
import {TickData} from "./TickData";
import {Observer} from "../technical/Observer";
import Stream = Highland.Stream;

export class StreamTickSource implements TickSource {
    private _stream: Stream<void>;

    private _onTickObserver: Observer<TickData>;
    private _onEndObserver: Observer<TickData>;

    constructor(stream: Stream<TickData>) {
        this._onTickObserver = new Observer<TickData>();
        this._onEndObserver = new Observer<TickData>();
        this._stream = StreamUtils.asyncMap(stream, async (e: TickData) => {
            await this._onTickObserver.trigger(e);
        });
    }

    public onTick(listener: (e: TickData) => Promise<void>): Function {
        return this._onTickObserver.subscribe(listener);
    }

    public async resume(): Promise<void> {
        this._stream.resume();
    }

    public async pause(): Promise<void> {
        this._stream.pause();
    }
}