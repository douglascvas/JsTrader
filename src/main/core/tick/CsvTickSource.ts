import {TickSource} from "./TickSource";
import * as fs from "fs";
import * as asStream from "highland";
import {DefaultTickData} from "./DefaultTickData";
import Stream = Highland.Stream;
import {TickData} from "./TickData";
import {Observer} from "../technical/Observer";
import {StreamUtils} from "../StreamUtils";

export class CsvTickSource implements TickSource {
    private _stream: Stream<void>;
    private _path: string;
    private _onTickObserver: Observer<TickData>;

    constructor(filePpath: string) {
        this._onTickObserver = new Observer<TickData>();
        if (!fs.existsSync(filePpath)) {
            throw new Error(`File does not exist: ${filePpath}`);
        }
        this._path = filePpath;
    }

    public onTick(listener: (e: TickData) => Promise<void>): Function {
        return this._onTickObserver.subscribe(listener);
    }

    public async resume(): Promise<void> {
        this._stream = this._stream || StreamUtils.asyncMap(await this.loadFile(), async (e: TickData) => {
            await this._onTickObserver.trigger(e);
        });
        this._stream.resume();
    }

    public async pause(): Promise<void> {
        this._stream.pause();
    }

    private async loadFile(): Promise<Stream<TickData>> {
        let stream: Stream<string> = asStream(fs.createReadStream(this._path, {
            highWaterMark: 64 * 1024,
            autoClose: true
        }));
        let linesStream = (<any>stream).split();
        let header = await linesStream.take(1).toPromise(Promise);

        let positions: Map<string, number> = new Map();
        header.split(',').forEach((value, index) => positions.set(value, index));
        let timeIndex = positions.get('time') || positions.get('date');
        let askIndex = positions.get('ask');
        let bidIndex = positions.get('bid');

        return linesStream
            .map(line => {
                let fields = line.split(',');
                return new DefaultTickData(new Date(fields[timeIndex]), parseFloat(fields[askIndex]), parseFloat(fields[bidIndex]));
            }).errors(e => {
                console.trace(e);
            });
    }
}