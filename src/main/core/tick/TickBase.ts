import {DefaultTickData} from "./DefaultTickData";
import {TickSource} from "./TickSource";
import Stream = Highland.Stream;
import {TickData} from "./TickData";

export interface TickBase {
    populate(tickSource: TickSource, tradeSymbol: string);
    ticksByDate(symbolCode: string, from: Date, to: Date): Promise<Stream<TickData>>;

}