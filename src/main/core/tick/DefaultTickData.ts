import {TickData} from "./TickData";

export class DefaultTickData implements TickData {
    private _time: Date;
    private _ask: number;
    private _bid: number;

    constructor(time: Date, ask: number, bid: number) {
        this._time = time;
        this._ask = ask;
        this._bid = bid;
    }

    get time(): Date {
        return this._time;
    }

    get ask(): number {
        return this._ask;
    }

    get bid(): number {
        return this._bid;
    }

}