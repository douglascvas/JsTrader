import {DefaultTickData} from "./DefaultTickData";
import * as asStream from "highland";
import {TickSource} from "./TickSource";
import {TickBase} from "./TickBase";
import {MongoManager} from "../database/MongoManager";
import Stream = Highland.Stream;

export class MongoTickBase implements TickBase {
    private _mongoManager: MongoManager;

    constructor(mongoManager: MongoManager) {
        this._mongoManager = mongoManager;
    }

    public async populate(tickSource: TickSource, tradeSymbol: string) {

        const col = await this._mongoManager.getCollection('ticks');
        let total = 0;

        let ticks = [];
        await tickSource.onTick(async tick => {
            try {
                // await col.bulkWrite(ticks,{ordered:false, w:1});
                // await col.updateMany(ticks, {$set: ticks}, {upsert: true});
                await col.insertOne(ticks);
                total += ticks.length;
                console.log("Inserted", total);
            } catch (e) {
                console.log(e);
            }
        });
        //     .map(tick => ({
        //     ...tick,
        //     _symbolCode: tradeSymbol
        //     // updateOne: {
        //     //     filter: {
        //     //         _symbolCode: tradeSymbol,
        //     //         _time: tick.time
        //     //     },
        //     //     update: {
        //     //         ...tick,
        //     //         _symbolCode: tradeSymbol
        //     //     },
        //     //     upsert: true
        //     // }
        // })).batch(50000), async (ticks: TickData[]) => {
        //     try {
        //         // await col.bulkWrite(ticks,{ordered:false, w:1});
        //         // await col.updateMany(ticks, {$set: ticks}, {upsert: true});
        //         await col.insertMany(ticks, {ordered: false});
        //         total += ticks.length;
        //         console.log("Inserted", total);
        //     }catch (e){
        //         console.log(e);
        //     }
        // }, async () => {
        //     console.log("Finished populating.");
        // }).resume();
    }

    public async ticksByDate(symbolCode: string, from: Date, to: Date): Promise<Stream<DefaultTickData>> {
        try {
            const col = await this._mongoManager.getCollection('ticks');
            const docs = await col.find({
                _symbolCode: symbolCode,
                _time: {
                    $gte: from,
                    $lte: to
                }
            }).stream().map(value => new DefaultTickData(value._time, value._ask, value._bid));
            return asStream(<any>docs);
        } catch (err) {
            console.log(err.stack);
            return null;
        }
    }
}