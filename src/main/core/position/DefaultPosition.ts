import {TradeType} from "../TradeType";
import {TradeUtils} from "../TradeUtils";
import {TradeSymbol} from "../public/TradeSymbol";
import {TradeContext} from "../public/TradeContext";
import {InternalPosition} from "./InternalPosition";
import {InternalTradeSymbol} from "../symbol/InternalTradeSymbol";
import {InternalTradeContext} from "../context/InternalTradeContext";
import {Account} from "../public/Account";
import {DefaultTradeSymbol} from "../symbol/DefaultTradeSymbol";

export class DefaultPosition implements InternalPosition {
    private _closedAt: Date;
    private _tradeSymbol: InternalTradeSymbol;
    private _comissionPerMillion: number;
    private _tradeContext: InternalTradeContext;

    private _tradeType: TradeType;
    private _volume: number;
    private _id: number;
    private _profit: number;
    private _entryPrice: number;
    private _stopLoss: number;
    private _takeProfit: number;
    private _swap: number;
    private _entryTime: Date;
    private _label: string;
    private _comment: string;

    public constructor(tradeType: TradeType,
                       symbol: TradeSymbol,
                       volume: number,
                       label: string,
                       tradeContext: TradeContext,
                       stopLossPips?: number,
                       takeProfitPips?: number,
                       comment?: string,
                       comissionPerMillion?: number) {
        stopLossPips = stopLossPips || 0;
        takeProfitPips = takeProfitPips || 0;
        this._entryPrice = tradeType.isBuy() ? symbol.price.ask : symbol.price.bid;
        this._tradeSymbol = <InternalTradeSymbol>symbol;
        this._tradeType = tradeType;
        this._volume = volume;
        this._label = label;
        this._stopLoss = tradeType.isBuy() ? this._entryPrice - TradeUtils.pipsToPrice(stopLossPips, symbol) : this._entryPrice + TradeUtils.pipsToPrice(stopLossPips, symbol);
        this._takeProfit = tradeType.isBuy() ? this._entryPrice + TradeUtils.pipsToPrice(takeProfitPips, symbol) : this._entryPrice - TradeUtils.pipsToPrice(takeProfitPips, symbol);
        this._comment = comment;
        this._comissionPerMillion = comissionPerMillion;
    }

    public close(): void {
        if (this._closedAt) {
            return;
        }
        this._closedAt = new Date();
        // create a static trade symbol
        this._tradeSymbol = new DefaultTradeSymbol((<DefaultTradeSymbol>this.tradeSymbol).tickSource, this._tradeSymbol);
        this._comissionPerMillion = Object.assign({}, this._comissionPerMillion);
    }

    get tradeType(): TradeType {
        return this._tradeType;
    }

    get volume(): number {
        return this._volume;
    }

    get id(): number {
        return this._id;
    }

    get profit(): number {
        return this._profit;
    }

    get grossProfit(): number {
        let price = 0;
        if (this._tradeType.isBuy()) {
            price = (this._tradeSymbol.price.bid - this._entryPrice) / this._tradeSymbol.price.bid;
        } else {
            price = (this.entryPrice - this._tradeSymbol.price.ask) / this._tradeSymbol.price.ask;
        }
        let profit = price * this._volume;
        return TradeUtils.round(profit);
    }

    get entryPrice(): number {
        return this._entryPrice;
    }

    get stopLoss(): number {
        return this._stopLoss;
    }

    get takeProfit(): number {
        return this._takeProfit;
    }

    get netProfit(): number {
        return this.grossProfit - this.commissions;
    }

    get swap(): number {
        return this._swap;
    }

    get commissions(): number {
        let commissionOverVolume = -1 * (this._volume * this._comissionPerMillion) / 1000000;
        let commissionOverSpread = -1 * (this.spread * this.volume);
        return TradeUtils.round(commissionOverSpread + commissionOverVolume);
    }

    get spread(): number {
        let spread = (this._tradeSymbol.price.ask - this._tradeSymbol.price.bid);
        return this._tradeType.isBuy() ? spread : -1 * spread;
    }

    get entryTime(): Date {
        return this._entryTime;
    }

    get pips(): number {
        return TradeUtils.priceToPips(this._tradeType.isBuy() ? this._entryPrice - this._tradeSymbol.price.bid : this._tradeSymbol.price.ask - this.entryPrice, this._tradeSymbol);
    }

    get label(): string {
        return this._label;
    }

    get comment(): string {
        return this._comment;
    }

    get tradeContext(): TradeContext {
        return this._tradeContext;
    }

    get tradeSymbol(): TradeSymbol {
        return this._tradeSymbol;
    }

    get account(): Account {
        return this._tradeContext.account;
    }

    get closedAt(): Date {
        return this._closedAt;
    }
}