import {Position} from "../public/Position";

export interface InternalPosition extends Position {
    close(): void;
}