import {ErrorCode} from "../ErrorCode";
import {PositionCloseEvent} from "../public/PositionCloseEvent";
import {Position} from "../public/Position";

export class DefaultPositionCloseEvent implements PositionCloseEvent {
    private _position: Position;
    private _error: ErrorCode;

    constructor(position: Position, error?: ErrorCode) {
        this._position = position;
        this._error = error;
    }

    get position(): Position {
        return this._position;
    }

    get error(): ErrorCode {
        return this._error;
    }
}