import {TradePrice} from "../public/TradePrice";

export class DefaultTradePrice implements TradePrice {
    private _ask: number;
    private _bid: number;

    constructor(ask: number, bid: number) {
        this._ask = ask;
        this._bid = bid;
    }

    public get ask(): number {
        return this._ask;
    }

    public get bid(): number {
        return this._bid;
    }
}