import {TradeSymbol} from "../public/TradeSymbol";
import {PriceChangedEvent} from "../public/PriceUpdateEvent";
import {TradePrice} from "../public/TradePrice";

export class DefaultPriceChangeEvent implements PriceChangedEvent {
    private _time: Date;
    private _tradeSymbol: TradeSymbol;
    private _newPrice: TradePrice;
    private _previousPrice: TradePrice;

    constructor(tradeSymbol: TradeSymbol, newPrice: TradePrice, previousPrice: TradePrice, time: Date) {
        this._tradeSymbol = tradeSymbol;
        this._newPrice = newPrice;
        this._previousPrice = previousPrice;
        this._time = time;
    }

    get tradeSymbol(): TradeSymbol {
        return this._tradeSymbol;
    }

    get newPrice(): TradePrice {
        return this._newPrice;
    }

    get previousPrice(): TradePrice {
        return this._previousPrice;
    }

    get time(): Date {
        return this._time;
    }
}