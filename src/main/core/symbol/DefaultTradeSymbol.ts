import {TradeSymbol} from "../public/TradeSymbol";
import {RoundingMode} from "../RoundingMode";
import {InternalTradeSymbol} from "./InternalTradeSymbol";
import {Partial} from "../technical/Partial";
import {TradePrice} from "../public/TradePrice";
import {Observer} from "../technical/Observer";
import {TickSource} from "../tick/TickSource";
import {DefaultTradePrice} from "../price/DefaultTradePrice";
import {TickData} from "../tick/TickData";

export class DefaultTradeSymbol implements InternalTradeSymbol {
    private _price: TradePrice;
    private _code: string;
    private _pipSize: number;
    private _digits: number;
    private _volumeMin: number;
    private _volumeMax: number;
    private _volumeStep: number;
    private _lotSize: number;
    private _tickSource: TickSource;
    private _onPriceChangeObserver: Observer<TickData>;
    private _onPriceChangeEndObserver: Observer<TickData>;
    public constructor(tickSource: TickSource, tradeSymbol: Partial<TradeSymbol> = {}) {
        this._price = tradeSymbol.price;
        this._code = tradeSymbol.code;
        this._pipSize = tradeSymbol.pipSize;
        this._digits = tradeSymbol.digits;
        this._volumeMin = tradeSymbol.volumeMin;
        this._volumeMax = tradeSymbol.volumeMax;
        this._onPriceChangeObserver = new Observer();
        this._onPriceChangeEndObserver = new Observer();
        this._tickSource = tickSource;
        this.useSource(tickSource);
    }

    public onPriceChangeEnd(listener: () => Promise<void>): Function {
        return this._onPriceChangeEndObserver.subscribe(listener);
    }

    public onPriceChange(listener: (e: TickData) => Promise<void>): Function {
        return this._onPriceChangeObserver.subscribe(listener);
    }

    public async resume(): Promise<void> {
        await this.tickSource.resume();
    }

    public async pause(): Promise<void> {
        await this.tickSource.pause();
    }

    private useSource(tickSource: TickSource): void {
        this._tickSource && this._tickSource.pause();
        this._tickSource = tickSource;
        this._tickSource.onTick(async tick => {
            this._price = new DefaultTradePrice(tick.ask, tick.bid);
            await this._onPriceChangeObserver.trigger(tick);
        });
    }

    get price(): TradePrice {
        return this._price;
    }

    get spread(): number {
        return this.price.ask - this.price.bid;
    }

    get code(): string {
        return this._code;
    }

    get pipSize(): number {
        return this._pipSize;
    }

    get digits(): number {
        return this._digits;
    }

    get volumeMin(): number {
        return this._volumeMin;
    }

    get volumeMax(): number {
        return this._volumeMax;
    }

    get volumeStep(): number {
        return this._volumeStep;
    }

    get lotSize(): number {
        return this._lotSize;
    }

    public normalizeVolume(volume: number, roundingMode: RoundingMode = RoundingMode.ToNearest): number {
        throw new Error('not implemented');
    }

    public quantityToVolume(quantity: number): number {
        throw new Error('not implemented');
    }

    public volumeToQuantity(volume: number): number {
        throw new Error('not implemented');
    }

    get tickSource(): TickSource {
        return this._tickSource;
    }

}