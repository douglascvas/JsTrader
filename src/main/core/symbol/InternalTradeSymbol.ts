import {TradeSymbol} from "../public/TradeSymbol";
import {RoundingMode} from "../RoundingMode";

export interface InternalTradeSymbol extends TradeSymbol {
    normalizeVolume(volume: number, roundingMode: RoundingMode): number;
    quantityToVolume(quantity: number): number;
    volumeToQuantity(volume: number): number;
}