export interface TradeSymbolTemplate {
    code: string;
    pipSize: number;
    digits: number;
    tickSize: number;
    volumeMin: number;
    volumeMax: number;
    lotSize: number;
}