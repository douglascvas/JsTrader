import {TradeSymbol} from "../public/TradeSymbol";

export interface SymbolTemplateLoader {
    loadSymbols(): Promise<Map<string, TradeSymbol>>;
}