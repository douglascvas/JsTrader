import {TradeSymbol} from "../public/TradeSymbol";
import {SymbolTemplateLoader} from "./SymbolTemplateLoader";

export class JsonSymbolLoader implements SymbolTemplateLoader {
    private _filePath: string;

    constructor(filePath: string){
        this._filePath = filePath;
    }

    public async loadSymbols(): Promise<Map<string, TradeSymbol>> {
        let tradeSymbolTemplateFromTradeSymbolCode: Map<string, TradeSymbol> = new Map();
        let templates = require(this._filePath);
        templates.forEach((template: TradeSymbol) => tradeSymbolTemplateFromTradeSymbolCode.set(template.code, template));
        return tradeSymbolTemplateFromTradeSymbolCode;
    }
}