export interface Session {
    token: string;
    account: Account;
}