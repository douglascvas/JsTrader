export class DefaultSession {
    private _token: string;
    private _account: Account;

    constructor(token: string, account: Account) {
        this._token = token;
        this._account = account;
    }

    get token(): string {
        return this._token;
    }

    get account(): Account {
        return this._account;
    }
}