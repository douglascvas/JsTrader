import {TradeContext} from "../public/TradeContext";
import {InternalAccount} from "./InternalAccount";
import {InternalTradeContext} from "../context/InternalTradeContext";
import {Position} from "../public/Position";
import {Account} from "../public/Account";
import {DefaultTradeContext} from "../context/DefaultTradeContext";
import {DefaultTradeSymbol} from "../symbol/DefaultTradeSymbol";
import {TickSource} from "../tick/TickSource";
import {TradeServer} from "../TradeServer";
import {Context} from "vm";

export class DefaultAccount implements InternalAccount {
    private _tradeContextFromContextId: Map<string, TradeContext>;
    private _server: TradeServer;

    private _balance: number;
    private _currency: string;
    private _isLive: boolean;
    private _leverage: number;
    private _accountId: string;
    private _brokerName: string;

    constructor(server: TradeServer, accountTemplate: Account = <Account>{}) {
        this._accountId = accountTemplate.accountId;
        this._currency = accountTemplate.currency || 'USD';
        this._leverage = accountTemplate.leverage;
        this._isLive = accountTemplate.isLive;
        this._server = server;
        this._tradeContextFromContextId = new Map<string, TradeContext>();
    }

    public async createContext(symbolCode: string, from?: Date, to?: Date): Promise<TradeContext> {
        let tradeSymbol = await this._server.createSymbol(symbolCode, await this._server.loadTicks(symbolCode, from, to));
        let context: TradeContext = new DefaultTradeContext(tradeSymbol, this, await this._server);
        this._tradeContextFromContextId.set(context.id, context);
        return context;
    }

    public getContext(contextId: string): TradeContext {
        return this._tradeContextFromContextId.get(contextId);
    }

    public getContexts(): TradeContext[] {
        return Array.from(this._tradeContextFromContextId.values());
    }

    get positions(): Position[] {
        let positions: Position[] = [];
        this._tradeContextFromContextId.forEach(c => positions.push(...c.positions));
        return positions;
    }

    get balance(): number {
        return this._balance;
    }

    get currency(): string {
        return this._currency;
    }

    get equity(): number {
        let totalProfit = this.getContexts()
            .map(context => context.positions)
            .reduce((profit, positions) => positions
                .map(p => (p.grossProfit)).reduce((prev, current) => prev + current, 0), 0);
        return this.balance - totalProfit;
    }

    get margin(): number {
        // TODO calculate this
        //volume / this.leverage * X/USD
        return 0;
    }

    get freeMargin(): number {
        // TODO calculate this
        return 0;
    }

    get marginLevel(): number {
        // TODO calculate this
        return 0;
    }

    get isLive(): boolean {
        return this._isLive;
    }

    get leverage(): number {
        return this._leverage;
    }

    get accountId(): string {
        return this._accountId;
    }

    get brokerName(): string {
        return this._brokerName;
    }

}