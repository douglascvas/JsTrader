import {Account} from "../public/Account";

export interface AccountService {
    loadAccountFromToken(accountId: string): Promise<Account>;
}