import {Collection, Db, MongoClient} from "mongodb";

export class MongoManager {
    private _db: Db;
    private _dbName: string;
    private _url: string;
    private _client: MongoClient;

    constructor(options: any = {}) {
        this._dbName = options.dbName = 'trade';
        this._url = options.url || `mongodb://localhost:27017/${this._dbName}`;
    }

    public async initialize(){
        await this.connect();
        const col = this._db.collection('ticks');
        await col.createIndex({_symbolCode: 1, _time: 1}, {unique: true, background: true, dropDups: true});
    }

    public async getCollection(name: string): Promise<Collection> {
        await this.connect();
        return this._db.collection(name);
    }

    private async connect(): Promise<void> {
        let connected = this._client && await this._client.isConnected(this._dbName);
        if (!connected) {
            this._client = await MongoClient.connect(this._url);
            this._db = this._client.db(this._dbName);
        }
    }
}