import {TradeType} from "../TradeType";
import {TradeResult} from "./TradeResult";
import {Position} from "../public/Position";
import {Account} from "../public/Account";
import {PendingOrder} from "../public/PendingOrder";
import {DefaultPendingOrder} from "../order/DefaultPendingOrder";
import {DefaultPosition} from "../position/DefaultPosition";
import {ErrorCode} from "../ErrorCode";
import {BinaryTree} from "../technical/BinaryTree";
import {Node} from "../technical/Node";
import {Observer} from "../technical/Observer";
import {TradeSymbol} from "../public/TradeSymbol";
import {InternalTradeContext} from "./InternalTradeContext";
import {InternalAccount} from "../account/InternalAccount";
import {InternalPendingOrder} from "../order/InternalPendingOrder";
import {InternalPosition} from "../position/InternalPosition";
import {PositionCloseEvent} from "../public/PositionCloseEvent";
import {PositionOpenEvent} from "../public/PositionOpenEvent";
import {DefaultPositionOpenEvent} from "../position/DefaultPositionOpenEvent";
import {DefaultPositionCloseEvent} from "../position/DefaultPositionCloseEvent";
import {TradeHistory} from "../history/TradeHistory";
import {DefaultTradeHistory} from "../history/DefaultTradeHistory";
import * as uuidv5 from 'uuid/v5';
import {TradeServer} from "../TradeServer";
import {TickData} from "../tick/TickData";

export class DefaultTradeContext implements InternalTradeContext {
    private _id: string;
    private _tradeSymbol: TradeSymbol;
    private _account: InternalAccount;
    private _server: TradeServer;
    private _orders: InternalPendingOrder[];
    private _positions: Position[];
    private _sellOrderTree: BinaryTree<InternalPendingOrder>;
    private _historyFromTimeframe: Map<number, TradeHistory>;

    private _buyOrderTree: BinaryTree<InternalPendingOrder>;
    private _sellStopLossTree: BinaryTree<InternalPosition>;

    private _sellTakeProfitTree: BinaryTree<InternalPosition>;
    private _buyStopLossTree: BinaryTree<InternalPosition>;
    private _buyTakeProfitTree: BinaryTree<InternalPosition>;
    private _onPositionOpenObserver: Observer<PositionOpenEvent>;
    private _onPositionCloseObserver: Observer<PositionCloseEvent>;

    public constructor(tradeSymbol: TradeSymbol,
                       account: Account,
                       server: TradeServer) {
        this._tradeSymbol = tradeSymbol;
        this._account = account;
        this._id = uuidv5(tradeSymbol.code, uuidv5.DNS);
        this._server = server;
        this._onPositionOpenObserver = new Observer();
        this._onPositionCloseObserver = new Observer();
        this._positions = [];
        this._orders = [];
        this._sellOrderTree = new BinaryTree<PendingOrder>();
        this._buyOrderTree = new BinaryTree<PendingOrder>();
        this._sellStopLossTree = new BinaryTree<InternalPosition>();
        this._sellTakeProfitTree = new BinaryTree<InternalPosition>();
        this._buyStopLossTree = new BinaryTree<InternalPosition>();
        this._buyTakeProfitTree = new BinaryTree<InternalPosition>();
        this._historyFromTimeframe = new Map();

        this.tradeSymbol.onPriceChange(async tick => {
            await this.handlePriceUpdate(tick);
        });
    }

    public async resume(): Promise<void> {
        await this.tradeSymbol.resume();
    }

    public async pause(): Promise<void> {
        await this.tradeSymbol.pause();
    }

    public async getOrCreateBarHistory(timeframeInMs: number): Promise<TradeHistory> {
        let timeframeHistory: TradeHistory = this._historyFromTimeframe.get(timeframeInMs);
        if (!timeframeHistory) {
            timeframeHistory = new DefaultTradeHistory(this.tradeSymbol, timeframeInMs);
            this._historyFromTimeframe.set(timeframeInMs, timeframeHistory);
        }
        return timeframeHistory;
    }

    public onPositionOpened(listener: (event: PositionOpenEvent) => void): Function {
        return this._onPositionOpenObserver.subscribe(listener)
    }

    public onPositionClosed(listener: (event: PositionCloseEvent) => void): Function {
        return this._onPositionCloseObserver.subscribe(listener)
    }

    public async executeMarketOrder(tradeType: TradeType,
                                    currency: TradeSymbol,
                                    volume: number,
                                    label: string,
                                    stopLossPips?: number,
                                    takeProfitPips?: number,
                                    comment?: string): Promise<TradeResult> {
        return this.createMarketOrder(tradeType, currency, volume, label, stopLossPips, takeProfitPips, comment);
    }

    public async placeStopOrder(tradeType: TradeType,
                                currency: TradeSymbol,
                                volume: number,
                                targetPrice: number,
                                label: string,
                                stopLossPips: number,
                                takeProfitPips: number): Promise<TradeResult> {
        let order: PendingOrder = new DefaultPendingOrder(tradeType, currency, volume, targetPrice, label, stopLossPips, takeProfitPips);
        this._orders.push(order);

        let error: ErrorCode = null;
        if ((volume % this._tradeSymbol.volumeMin) > 0)
            error = ErrorCode.BadVolume;
        else if (this._account.balance <= 0)
            error = ErrorCode.NoMoney;

        return new TradeResult(true, error, null, order);
    }

    private async handlePriceUpdate(tickData: TickData): Promise<void> {
        await this.resolvePendingOrders(tickData);
        await this.resolveSellPositions(tickData);
        await this.resolveBuyPositions(tickData);
    }

    public async closePosition(position: Position): Promise<void> {
        let internalPosition: InternalPosition = <InternalPosition>position;
        if (internalPosition.closedAt) {
            return;
        }
        internalPosition.close();
        if (position.tradeType == TradeType.Sell) {
            this._sellStopLossTree.remove(internalPosition);
            this._sellTakeProfitTree.remove(internalPosition);
        } else {
            this._buyStopLossTree.remove(internalPosition);
            this._buyTakeProfitTree.remove(internalPosition);
        }
        this._positions.splice(this._positions.indexOf(position), 1);
        await this._onPositionCloseObserver.trigger(new DefaultPositionCloseEvent(position));
    }

    private async createMarketOrder(tradeType: TradeType,
                                    currency: TradeSymbol,
                                    volume: number,
                                    label: string,
                                    stopLossPips?: number,
                                    takeProfitPips?: number,
                                    comment?: string,
                                    order?: PendingOrder): Promise<TradeResult> {


        let position: InternalPosition = new DefaultPosition(tradeType, currency, volume, label, this, stopLossPips, takeProfitPips, comment, await this._server.getComissionPerMillion());
        this._positions.push(position);
        if (tradeType === TradeType.Buy) {
            this._buyStopLossTree.add(position.stopLoss, position);
            this._buyTakeProfitTree.add(position.takeProfit, position);
        } else {
            this._sellStopLossTree.add(position.stopLoss, position);
            this._sellTakeProfitTree.add(position.takeProfit, position);
        }
        await this._onPositionOpenObserver.trigger(new DefaultPositionOpenEvent(position));
        return new TradeResult(true, null, position, order);

    }

    private async resolveBuyPositions(tickData: TickData) {
        // Close the buy _orders whose stop loss are below the ask price
        await this.resolveBuyPositionsByStopLoss(tickData.ask);

        // Close the buy _orders whose take profit are above the bid price
        await this.resolveBuyPositionsByTakeProfit(tickData.bid);
    }

    private async resolveBuyPositionsByTakeProfit(bid: number) {
        let targetPositions: Node<Position> = this._buyTakeProfitTree.findSmallerThan(bid, true);
        if (targetPositions) {
            let operations = targetPositions.toList()
                .map(node => node.value)
                .map(position => this.closePosition(position));
            await Promise.all(operations);
        }
    }

    private async resolveBuyPositionsByStopLoss(ask: number) {
        let targetPositions: Node<Position> = this._buyStopLossTree.findGreaterThan(ask, true);
        if (targetPositions) {
            let operations = targetPositions.toList()
                .map(node => node.value)
                .map(position => this.closePosition(position));
            await Promise.all(operations);
        }
    }

    private async resolveSellPositions(tickData: TickData) {
        // Close the sell _orders whose stop loss are greater the bid price
        await this.resolveSellPositionsByStopLoss(tickData.bid);

        // Close the sell _orders whose take profit are below the ask price
        await this.resolveSellPositionsByTakeProfit(tickData.ask);
    }

    private async resolveSellPositionsByTakeProfit(ask: number): Promise<void> {
        let targetPositions: Node<Position> = this._sellTakeProfitTree.findSmallerThan(ask, true);
        if (targetPositions !== null) {
            let operations = targetPositions.toList()
                .map(node => node.value)
                .map(position => this.closePosition(position));
            await Promise.all(operations);
        }
    }

    private async resolveSellPositionsByStopLoss(bid: number): Promise<void> {
        let targetPositions: Node<Position> = this._sellStopLossTree.findGreaterThan(bid, true);
        if (targetPositions !== null) {
            let operations = targetPositions.toList()
                .map(node => node.value)
                .map(position => this.closePosition(position));
            await Promise.all(operations);
        }
    }

    private async resolvePendingOrders(tickData: TickData): Promise<void> {
        await this.resolveSellPendingOrders(tickData.ask);
        await this.resolveBuyPendingOrders(tickData.bid);
    }

    private async resolveBuyPendingOrders(bid: number) {
        let buyNode: Node<PendingOrder> = this._buyOrderTree.findGreaterThan(bid, true);
        if (buyNode) {
            let buyNodes: Node<PendingOrder>[] = buyNode.toList();
            let ordersResult = buyNodes.map(node => node.value)
                .map(order => this.executePendingOrder(order));
            await Promise.all(ordersResult);
        }
    }

    private async resolveSellPendingOrders(ask: number): Promise<void> {
        let sellNode: Node<PendingOrder> = this._sellOrderTree.findSmallerThan(ask, true);
        if (sellNode) {
            let sellNodes: Node<PendingOrder>[] = sellNode.toList();
            let ordersResult = sellNodes.map(node => node.value)
                .map(order => this.executePendingOrder(order));
            await Promise.all(ordersResult);
        }
    }

    private async executePendingOrder(order: PendingOrder): Promise<TradeResult> {
        return this.executeMarketOrder(order.tradeType, this._tradeSymbol, order.volume, order.label, order.stopLossPips, order.takeProfitPips, order.comment)
    }

    get tradeSymbol(): TradeSymbol {
        return this._tradeSymbol;
    }

    get positions(): Position[] {
        return this._positions;
    }

    get orders(): PendingOrder[] {
        return this._orders;
    }

    get account(): Account {
        return this._account;
    }

    get id(): string {
        return this._id;
    }
}