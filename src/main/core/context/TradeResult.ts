import * as util from "util";
import {ErrorCode} from "../ErrorCode";
import {PendingOrder} from "../public/PendingOrder";
import {Position} from "../public/Position";

export class TradeResult
{
    constructor(public isSuccessfull: boolean,
                public error: ErrorCode,
                public position: Position,
                public pendingOrder: PendingOrder)
    {
    }

  public toString()
{
    if (!this.isSuccessfull)
        return util.format("TradeResult (Error: %s)", this.error == null ? "" : this.error.name);
    let stringList: string[] = ["Success"];
    if (this.position != null)
        stringList.push(util.format("Position: PID%s", this.position.id));
    if (this.pendingOrder != null)
        stringList.push(util.format("PendingOrder: OID%s", this.pendingOrder.id));
    return util.format("TradeResult (%s)", stringList.join(", "));
}
}