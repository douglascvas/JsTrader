export class TradeType {
    public static readonly Buy = new TradeType("Buy");
    public static readonly Sell = new TradeType("Sell");

    constructor(public name: string){
    }

    public isBuy(): boolean {
        return this === TradeType.Buy;
    }
}